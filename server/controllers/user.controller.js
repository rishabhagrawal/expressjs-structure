var Q = require('q');
var User = require('./../models/user.model');

module.exports = {

    //create new User
    saveData: function(req, res) {
        var deferred = Q.defer();
        var newUser = new User(req);
        newUser.save(function(err, data) {
            if (err) {
                // console.log(err);
            } else {
                // console.log(data);
            }
        }).then(function(result) {
            deferred.resolve(result);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    },

    getAllUsers: function(req, res) {
        var deferred = Q.defer();
        User.find({})
            .sort({ 'created_at': -1 })
            .exec(function(err, users) {
                if (err) {
                    // console.log(err);
                } else {
                    // console.log(users);
                }
            }).then(function(result) {
                deferred.resolve(result);
            }, function(error) {
                deferred.reject(error);
            });
        return deferred.promise;
    }

};
