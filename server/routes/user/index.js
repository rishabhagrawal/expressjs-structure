var express = require('express');
var router = express.Router();

var createRouter = require('./create');
router.use('/new', createRouter);

var userListRouter = require('./list');
router.use('/', userListRouter);

module.exports = router;
