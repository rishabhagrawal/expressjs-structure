var Q = require('q');
var express = require('express');
var router = express.Router();
var User = require('./../../controllers/user.controller');

router.route('/')
    .post(function(req, res) {
        var userData = {
            name: (req.body.name),
            username: req.body.username,
            age: req.body.age,

        }
        User.saveData(userData).then(function(result){
            console.log("*********Result*********");
            console.log(result);
            console.log("*********Result*********");
        	// res.json(result);
            var userdata = result;
            res.redirect('/user');
        }, function(error){
            console.log("*********Error*********");
            console.log(error);
            console.log("*********Error*********");
            // res.session['success'] = 'User added successfully';
        	// res.status('404');
            res.render('./user/new-user');
        })
    })

    .get(function(req, res){
        // res.cookie('username', 'Rishabh Agrawal');
    	// res.cookie('password', 'Rishabh',{ maxAge: 9000, httpOnly: true });
        res.render('./user/new-user');

    });

module.exports = router;
