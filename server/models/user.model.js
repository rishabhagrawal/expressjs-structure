var mongoose = require('mongoose');
var schema = mongoose.Schema;

// custom error messages
var max = [100, "The value of path '{PATH}' ({VALUE}) exceeds the limit ({MAX})."];
// ValidationError: The value of path age' (4) exceeds the limit (100).


var userSchema = new schema({
    name: { type: String },
    username: { type: String, trim: true },
    age: { type: Number, max: max },
    status: { type: Boolean, default: 1 }
}, {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

// userSchema.pre('save', function(next){
// 	var doc = this;
// 	User.findByIdAndUpdate({_id : '575a75991d87d7a93a9c8e1c'}, {$inc: { seq: 1} }, function(err, data){
// 		if(err){
// 			return next(err);
// 		}
// 		next();
// 	})
// })


var User = mongoose.model('user', userSchema);
module.exports = User;
