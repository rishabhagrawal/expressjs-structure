var bcrypt = require('bcrypt');
var salt_factor = 10;

module.exports = {
    encrytion: function(password, callback) {
        bcrypt.genSalt(salt_factor, function(err, salt) {
            if (err) {
               callback(err);
            } else {
                bcrypt.hash(password, salt, function(err, hash) {
                    if (err) {
                        callback(err);
                    } else {
                        callback(hash);
                    }
                })
            }
        });
    },

    compare: function(password, dbpassword, callback) {
        bcrypt.compare(password, dbpassword, function(err, res) {
            if (err) {
                callback(err);
            } else {
                callback(res);
            }
        })
    }

};
