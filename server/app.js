// call the packages we need
var express = require('express'); // call express js
var app = express();
var port = process.env.PORT || 9001; // set our app port
var morgan = require('morgan');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var dbConfig = require('./config/database');
var router = require('./routes');
var path = require('path');


//***** Log File ******
app.use(morgan('dev'));
//***** Log File ******



//***** Database Connectivity *******
mongoose.connect(dbConfig.database);
//***** Database Connectivity *******


//****** Cookie Parser ********
app.use(cookieParser('test'));
app.use(session({
	secret : "@nonymous"
}))
//****** Cookie Parser ********


//******* view engine and static files *******
app.use(express.static('www'));
app.set('views', './views');
app.set('view engine', 'jade');
// app.locals.basedir = path.join(__dirname);
//******* view engine and static files *******


//****** Body Parser *******
// configure app to use bodyparser()
// this will let us get the data form a Post
app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());
//****** Body Parser *******


//****** Router *******
app.use(router);
//****** Router *******

//***** Listen *****
//port calling
var server = app.listen(port, function() {
        console.log('Server is running on port : http://localhost:' + port);
    })
    //***** Listen *****
