var gulp = require('gulp');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var nodemon = require('gulp-nodemon');
var plumber = require('gulp-plumber');
var sass = require('gulp-sass');
var notify = require('gulp-notify');
var concat = require('gulp-concat');
var jsValidate = require('gulp-jsvalidate');
var minifyCSS = require('gulp-cssnano');
var uglify = require('gulp-uglify');

var config = {
    "fonts": [
        './public/fonts/**/*.*',
        './node_modules/bootstrap/fonts/*.*',
        './node_modules/roboto-fontface/fonts/roboto/*.*'
    ],
    'vendorcss': [
        './node_modules/bootstrap/dist/css/bootstrap.min.css'
    ],
    'vendorjs': [
        './node_modules/jquery/dist/jquery.min.js',
        './node_modules/bootstrap/dist/js/bootstrap.min.js'
    ]
}

gulp.task('browser-sync', ['nodemon'], function() {
    browserSync({
        proxy: "localhost:9001", // local node app address
        port: 5000, // use *different* port than above
        notify: false
    });
});

gulp.task('nodemon', function(cb) {
    var called = false;
    return nodemon({
            script: 'server/app.js',
            ignore: [
                'gulpfile.js',
                'node_modules/'
            ]
        })
        .on('start', function() {
            if (!called) {
                called = true;
                cb();
            }
        })
        .on('restart', function() {
            setTimeout(function() {
                reload({ stream: false });
            }, 1000);
            gulp.src('server/app.js')
                .pipe(notify('Reloading page, please wait...'));
        });
});

//=============
// Custom SASS
//=============
// Custome Sass Compile and concatinate..
gulp.task('sass', function() {
    gulp.src('./public/styles/style.scss')
        .pipe(plumber({
            errLogToConsole: true,
            errorHandler: notify.onError({
                "title": "Sass Error",
                "message": "Sass Error"
            })
        }))
        .pipe(sass())
        .on("error", console.log)
        .pipe(concat('app.min.css'))
        .pipe(gulp.dest('./www/css/'));
});

//=============
// Vendor CSS
//=============
//vendor css compile and minified
gulp.task('vendor-css', function() {
    gulp.src(config.vendorcss)
        .pipe(minifyCSS())
        .pipe(concat('vendor.min.css'))
        .pipe(gulp.dest('./www/css/'));
});

//=============
// Fonts
//=============
gulp.task('fonts', function() {
    gulp.src(config.fonts)
        .pipe(gulp.dest('./www/fonts/'));
});


//=============
// Images Move
//=============
gulp.task('images', function() {
    gulp.src('./public/images/**/**/**/*.*')
        .pipe(gulp.dest('./www/images/'));
});


//=============
// Custom JS
//=============
gulp.task('app-js', function() {
    gulp.src('./public/scripts/**/**/*.js')
        .pipe(plumber({
            errLogToConsole: true,
            errorHandler: notify.onError({
                "title": "Javascript Error",
                "message": "Javascript Error"
            })
        }))
        .pipe(concat('app.min.js'))
        .pipe(jsValidate())
        .on("error", console.log)
        .pipe(gulp.dest('./www/js/'));
});


//=============
// Vendor Js
//=============
gulp.task('vendor-js', function() {

    gulp.src(config.vendorjs)
        .pipe(uglify())
        .pipe(concat('vendor.min.js'))
        .pipe(gulp.dest('./www/js/'));
});



gulp.task('default', ['browser-sync', 'sass', 'vendor-css', 'app-js', 'vendor-js', 'fonts', 'images'], function() {

    gulp.watch('./public/styles/**/**/*.scss', function() {
        gulp.run('sass');
    });

    gulp.watch(['./views/**/**/*.jade'], reload);
    gulp.watch(['./server/**/**/*.js'], reload);
    gulp.watch(['./public/styles/**/**/*.scss'], reload);
    gulp.watch(['./public/scripts/**/**/*.js'], reload);
});
